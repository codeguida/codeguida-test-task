# Сodeguida Test Task

## Test task

To create html/scss layout for Sweeties block (without Footer part).
The repo code is just hello world vue.js app created via cli-vue. You already could use SCSS from the box.

1. Design is in [Figma](https://www.figma.com/file/IM3818jN3ITHRTNdKhRNxDQF/codeguida_test?node-id=0%3A1)
2. Here are fonts: [Google Drive](https://drive.google.com/drive/folders/11F92Iv3xPVdXw3zEI1pc5clfmosg-qOk)
3. You need to fork this repo and do work in the fork
4. After finishing the task you will need to send a link to the forked repo

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
